import db
import os
from datetime import datetime
import PySimpleGUI as sg

def registration():
    password = input("Type admin password to enter admin mode: ")
    query = db.con.cursor()
    query.execute("SELECT ID, name, surname, password FROM users WHERE id=1")
    user = query.fetchone()
    if user[3] == password:
        print("Valid password. Do You want to add a new user?(Y/N) ")
        user_selection = input()
    if user_selection == "N" or user_selection == "n":
        start()
    elif user_selection == "Y" or user_selection == "y":
        name = input("Enter name: ")
        surname = input("Enter surname: ")
        password = input("Enter password: ")
        query.execute("INSERT INTO users (name, surname, password) VALUES (%s, %s, %s)", (name, surname, password))
        print("You succesfully created a user!")
    else:
        print("Wrong answer please log in once again!")
        registration()
    db.con.commit()
    start()

def login():
    query = db.con.cursor()
    query.execute("SELECT * FROM users")
    rows = query.fetchall()
    print("Sign in. Available users: ")
    for r in rows:
        print(f"{r[1]}")
    print("---------------------------")
    account_name = input("Choose account:")
    query.execute(f"SELECT id, name, surname, password FROM users WHERE name='{account_name}'")
    user = query.fetchone()
    if user is None:
        print(f"There is no user with name {account_name}. Log in one more time!")
        login()
    for i in range(3):
        password = input("Enter password: ")
        if user[3] == password:
            print("You have just logged in")
            print("---------------------------")
            start()
            break
        else:
            print("Wrong password!")        
        print(f"You have {2-i} attempts")
    login()

def adding_items():
    print("Adding item")
    name = input("Name: ")
    product_code = input("Product code:")
    amount = input("Amount: ")
    price = input("Price [zł]: ")
    margin = input("Margin [%]: ")
    vat = input("vat [%]: ")
    query = db.con.cursor()
    query.execute("INSERT INTO goods (name, amount, price, margin, vat, code) values (%s, %s, %s, %s, %s, %s)", (name, amount, price, margin, vat, product_code))
    db.con.commit()
    user_selection = input("Do You want to add more items?(Y/N) ")
    if user_selection == "N" or user_selection == "n":
        transactions()
    elif user_selection == "Y" or user_selection == "y":
        adding_items()
    else:
        user_selection = input("Do You want to add more items?(Y/N) ")

def sell_items():
    invoice_buyer = input("Invoice buyer: ")
    query = db.con.cursor()
    #nadawnie numeru faktury
    query.execute("SELECT invoice_number FROM transactions")
    transaction_rows = query.fetchall()
    invoice_number = 1
    for r in transaction_rows:
        invoice_number = r[0]
    invoice_number +=1

    query.execute("SELECT name, amount, price, margin, vat, code FROM goods")
    rows = query.fetchall()
    summary_invoice =[]
    while True:
        user_selection = input("Enter product code ")
        for r in rows:
            if user_selection in r[5]: 
                print(f"name:{r[0]} amount:{r[1]} price:{r[2]}zł margin:{r[3]}% product code:{r[5]}")
        item_code = input("Product code: ")
        for r in rows:
            if item_code==r[5]:
                item_amount = input("Amount: ")
                item_margin = input(f"Margin:{r[3]}%")
                if item_margin == '':
                    item_margin = r[3]
                # 23% -> 0.01 * 23
                final_price = round(float(r[2])*(1+0.01*float(item_margin))*(1+ 0.01*float(r[4])), 2)

                item_price = input(f"Price:{final_price}zl")
                if item_price == '':
                    item_price = float(final_price)
                final_item_amount=r[1]-int(item_amount)
                query.execute(f"UPDATE goods SET Amount={final_item_amount} WHERE code='{item_code}'")
                db.con.commit()
                sold_item = {"name":r[0], "amount":item_amount, "price":item_price, "vat":r[4], "total":round(float(item_amount)*item_price,2)}
                summary_invoice.append(sold_item)
                print("Product added to invoice")
                transaction_date = datetime.today().strftime('%Y-%m-%d')
                query.execute("INSERT INTO transactions (name, amount, price, type, invoice_number, vat, buyer, date) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (r[0], item_amount, float(item_price), "sell", invoice_number, r[4], invoice_buyer, transaction_date))
                db.con.commit()
                user_selection = input("Do You want to sell more items?(Y/N)")
        if user_selection == "N" or user_selection == "n":
            total_price = 0
            for x in summary_invoice:
                total_price+=round(float(x["total"]),2)
                print(f"Name:{x['name']} | Amount:{x['amount']} | Price:{x['price']}zl | Vat:{x['vat']}% | Total:{x['total']}zl")
            print(f"Summary:{total_price}zl")
            transactions()
            break

def show_invoice():
    query = db.con.cursor()
    query.execute("SELECT id, name, amount, price, type, invoice_number FROM transactions")
    transactions_rows = query.fetchall()
    invoice_number_list = []
    print("Avaiable VAT Invoice")
    for r in transactions_rows:
        if r[5] not in invoice_number_list:
            invoice_number_list.append(r[5])
    print(invoice_number_list)
    invoice_number = input("Which number of invoice to check? ")
    query.execute(f"SELECT id, name, amount, price, type, invoice_number, vat, buyer FROM transactions WHERE invoice_number={invoice_number}")
    invoice = query.fetchall()
    invoice_print(invoice_number, invoice)
    
        
def invoice_print(invoice_number, invoice):
    invoice_name = f"FA{invoice_number}.txt"
    invoice_date = datetime.today().strftime('%Y-%m-%d')
    file_to_print = open(invoice_name, "w")
    street_name = 'Wawelska 104'

    invoice_date = datetime.today().strftime('%Y-%m-%d')
    file_to_print = open(invoice_name, "w")
    file_to_print.write("--------------------------------------------------------------------------------\n")
    file_to_print.write(f"| Trading Company VESCA BIS S.C              |  Invoice VAT                    |\n")
    file_to_print.write(f"| Wawelska 104    64-920 Pila                |                                 |\n")
    phone_number = "Phone: 607 291 013"
    invoice_number_to_print = f"Invoice number: {invoice_number}/FA"
    for i in range(80):
        if i == 0 or i == 44-len(phone_number):
            file_to_print.write(f"|")
        if i == 1:
            file_to_print.write(f"{phone_number}")
        if i == 46-len(phone_number):
            file_to_print.write(f"{invoice_number_to_print}")
        if i == 77-len(phone_number)-len(invoice_number_to_print):
            file_to_print.write(f"|")
        else:
            file_to_print.write(" ")
    file_to_print.write("\n")
    file_to_print.write(f"| NIP: 764-23-47-430                         |  Date: {invoice_date}               |\n")
    file_to_print.write("--------------------------------------------------------------------------------\n")
    file_to_print.write(f"| Seller:                                    |  Buyer:                         |\n")
    for i in range(80):
        if i == 0 or i == 44-len(street_name):
            file_to_print.write(f"|")
        if i == 1:
            file_to_print.write(f"{street_name}")
        if i == 46-len(street_name):
            file_to_print.write(f"{invoice[0][7]}")
        if i == 77-len(street_name)-len(invoice[0][7]):
            file_to_print.write(f"|")
        else:
            file_to_print.write(" ")
    file_to_print.write("\n")
    file_to_print.write(f"| 64-920 Pila                                |  14 Lutego 18                   |\n")
    file_to_print.write(f"| NIP: 764-23-47-430                         |  64-920 Pila                    |\n")
    file_to_print.write("--------------------------------------------------------------------------------\n")
    file_to_print.write("Payment method: by cash\n")
    file_to_print.write("Payment date: 30/08/2022                        Comments:\n")
    file_to_print.write("--------------------------------------------------------------------------------\n")



    total_price = 0
    i=0
    for r in invoice:
        file_to_print.write(f"No:{i+1} | Name:{r[1]} | Amount:{r[2]} | Price:{round(r[3], 2)}zl | Vat:{r[6]}% | Total:{r[3]*r[2]}zl\n")
        total_price+=round(float(r[3]*r[2]),2)
        i+=1
    file_to_print.write("--------------------------------------------------------------------------------\n")
    file_to_print.write(f"Total price: {total_price}zl\n")
    file_to_print.write(f"Seller signature: __________________                   \n")
    file_to_print.write("--------------------------------------------------------------------------------\n")
    file_to_print.write("Invoice seller: Grzegorz Drozda\n")
    file_to_print.write("Confirmation of receipt of the invoice: _________________    __________________\n")
    file_to_print.write("                                         date of invoice      buyer signature\n\n\n\n")
    file_to_print.write("                            Grzegorz Drozda program v.1.01 Serial Number:723241")
    file_to_print.close()

    invoice_file_name = f"FA{invoice_number}"
    file_path = "C:\\Users\\Wojtek\Desktop\\Projekt python\\"+invoice_file_name + ".txt"
    with open(file_path, 'r') as f:
        print(f.read())
    user_selection = input("Do You want to print invoice?(Y/N)")
    if user_selection == "Y" or user_selection == "y":
        # real print
        # os.startfile(file_path, "print")
        transactions()
    else:
        transactions()



def item_update():
    query = db.con.cursor()
    query.execute("SELECT name, amount, price, margin, vat, code FROM goods")
    rows = query.fetchall()
    user_selection = input("Search for items by product code: ")
    for r in rows:
        if user_selection in r[5]: 
            print(f"name:{r[0]} amount:{r[1]} price:{r[2]}zł margin:{r[3]}% product code:{r[5]}")   

    item_code = input("Type exact product code: ")     
    for r in rows:        
        if item_code==r[5]:
            item_amount = input(f"Amount:{r[1]} ")
            if item_amount == '':
                item_amount = r[1]
            item_margin = input(f"Margin:{r[3]}% ")
            if item_margin == '':
                item_margin = r[3]
            item_price = input(f"Price:{r[2]}zl ")
            if item_price == '':
                item_price = float(r[2])
            item_vat = input(f"Vat:{r[4]} ")
            if item_vat == '':
                item_vat = r[4]
            if item_amount == '' or item_margin == '' or item_price == '' or item_vat == '':
                item_update()
            query.execute(f"UPDATE goods SET Amount={item_amount}, Margin={item_margin}, Price={item_price}, Vat={item_vat} WHERE code='{item_code}'")
            db.con.commit()
            print("Update item is succesful!")
            warehouse()
    print(f"There is no product with this code:{item_code}")
    item_update()
    
def item_delete():
    query = db.con.cursor()
    query.execute("SELECT name, amount, price, margin, vat, code FROM goods")
    rows = query.fetchall()
    user_selection = input("Search for items by product code: ")
    for r in rows:
        if user_selection in r[5]: 
            print(f"name:{r[0]} amount:{r[1]} price:{r[2]}zł margin:{r[3]}% product code:{r[5]}")
    item_code = input("Type exact product code: ")     
    for r in rows:        
        if item_code==r[5]:
            query.execute(f"DELETE FROM goods WHERE code='{item_code}'")
            db.con.commit()
            print("Delete item is succesful!")
            warehouse()
    print(f"There is no product with this code:{item_code}")
    item_delete()

def items_show():
    query = db.con.cursor()
    query.execute("SELECT name, amount, price, margin, vat , code FROM goods")
    rows = query.fetchall()
    for r in rows:
          print(f"name:{r[0]} amount:{r[1]} price:{r[2]}zł margin:{r[3]}% vat:{r[4]}% product code:{r[5]}")
    warehouse()

def receive_items():
    invoice_seller = input("Invoice seller: ")
    query = db.con.cursor()
    query.execute("SELECT name, amount, price, margin, vat, code FROM goods")
    rows = query.fetchall()
    invoice_number = input("Invoice number: ")
    invoice_type = "buy"
    invoice_date = datetime.today().strftime('%Y-%m-%d')
    while True:
        is_good_code = False
        user_selection = input("Search for items by product code: ")
        for r in rows:
            if user_selection in r[5]: 
                print(f"name:{r[0]} amount:{r[1]} price:{r[2]}zł margin:{r[3]}% product code:{r[5]}")
        item_code = input("Type exact product code: ")            
        for r in rows:        
            if item_code==r[5]:
                is_good_code = True
        if is_good_code == False:
            add_new_product = input("There is no product with this code. Do You want to add it?(Y/N)")
            if add_new_product == "y" or add_new_product == "Y":
                name = input("Name: ")
                product_code = input("Product code:")
                amount = input("Amount: ")
                price = input("Price [zł]: ")
                margin = input("Margin [%]: ")
                vat = input("vat [%]: ")
                query = db.con.cursor()
                query.execute("INSERT INTO goods (name, amount, price, margin, vat, code) values (%s, %s, %s, %s, %s, %s)", (name, amount, price, margin, vat, product_code))
                db.con.commit()
                query.execute("INSERT INTO receive_invoice (name, amount, price, type, invoice_number, vat, seller, date) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (name, amount, float(price), invoice_type, invoice_number, vat, invoice_seller, invoice_date))
                db.con.commit()
                print("Product added to invoice")
        for r in rows:        
            if item_code==r[5]:
                name = r[0]
                amount = input("Amount: ")
                price = input("Price [zł]: ")
                vat = r[4]
                query.execute(f"UPDATE goods SET Amount={r[1]+int(amount)}, price={price} WHERE code='{item_code}' ")
              
                query.execute("INSERT INTO receive_invoice (name, amount, price, type, invoice_number, vat, seller, date) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (name, amount, float(price), invoice_type, invoice_number, vat, invoice_seller, invoice_date))
                print("Product added to invoice")
                db.con.commit()
        user_selection = input("Do You want to add more items to invoice?(Y/N)")
        if user_selection == "N" or user_selection == "n":
            break
    print("Invoice added successfully!")
    transactions()
   
def sorting_sold_items(item):
    return item['total']

def selling_analysis():
    query = db.con.cursor()
    query.execute("SELECT name, SUM(amount), price FROM transactions GROUP BY name, price")
    rows = query.fetchall()
    print("Summary of selling:")
    list_of_items = []
    for r in rows:
        sum = float(r[1])*float(r[2])
        sold_item = {"name":r[0], "amount":r[1], "price":r[2], "total":sum}
        list_of_items.append(sold_item)
    
    list_of_items.sort(key=sorting_sold_items, reverse=True)
    for i in list_of_items:
        print(i)
    print("---------------------------")
    start()

        



    



def transactions():
    print("---------------------------")
    print("1.Issue VAT Invoice")
    print("2.Receive VAT Invoice")
    print("3.Show Issue VAT Invoice")
    print("9.Get back to main menu")
    print("---------------------------")
    user_selection = input()
    match user_selection:
        case '1':
            sell_items()
        case '2':
            receive_items()
        case '3':
            show_invoice()
        case '9':
            start()
        case _:
            transactions()
            
def warehouse():
    print("---------------------------")
    print("1.Update item in warehouse")
    print("2.Delete item in warehouse")
    print("3.Show all items in warehouse")
    print("4.Add an item to warehouse")
    print("9.Get back to main menu")
    print("---------------------------")
    user_selection = input()
    match user_selection:
        case '1':
            item_update()
        case '2':
            item_delete()
        case '3':
            items_show()
        case '4':
            adding_items()
        case '9':
            start()
        case _:
            warehouse()

def start():
    print("Choose what to do:")
    print("1.Transactions")
    print("2.Warehouse")
    print("7.Selling analysis")
    print("8.Change user")
    print("9.Admin mode")
    print("k.Quit the program")
    print("---------------------------")
    user_selection = input("What do You want to do? ")
    match user_selection:
        case '1':
            transactions()
        case '2':
            warehouse()
        case '7':
            selling_analysis()
        case '8':
            print("---------------------------")
            print("Logging off [...]")
            print("---------------------------")
            return 0
        case '9':
            registration()
        case 'k':
            exit()
        case _:
            start()
# login()
# start()

sg.theme('DarkAmber')   # Add a touch of color
# All the stuff inside your window.
layout = [  [sg.Text('Some text on Row 1')],
            [sg.Text('Enter something on Row 2'), sg.InputText()],
            [sg.Button('Ok'), sg.Button('Cancel')] ]

# Create the Window
window = sg.Window('Window Title', layout)
# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Cancel': # if user closes window or clicks cancel
        break
    print('You entered ', values[0])

window.close()


db.con.close()
#TODO


    